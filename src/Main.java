public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());
        Labrador dog2 = new Labrador("puccino", "blue");
        System.out.println(dog2.name + dog2.speak());
        Yorkshire dog3 = new Yorkshire("pluto");
        System.out.println(dog3.name + dog3.speak());
        System.out.println(dog2.avgBreedWeight());
        Cage bho = new Cage();
        bho.add(dog);
        bho.add(dog2);
        bho.add(dog3);
        bho.remove(dog3);
        bho.chaos();
        System.out.println(bho);
        Labrador dog4 = new Labrador("pippo", "blue");
        Labrador dog5 = new Labrador("pippo", "blue");
        Labrador dog6 = new Labrador("pippo", "blue");
        LabradorCage cage2= new LabradorCage();
        cage2.add(dog4);
        cage2.add(dog5);
        cage2.add(dog6);
        System.out.println(cage2);
    }
}
