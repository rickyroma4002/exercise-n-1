import java.lang.reflect.Array;
import java.util.ArrayList;

public class Cage {
    ArrayList<Dog> cage = new ArrayList<Dog>(16);

    public Cage(){}
    public void add(Dog dog){
        cage.add(dog);
    }
    public void remove(Dog dog){
        cage.remove(dog);
    }
    public void chaos(){
        for (int i=0; i<cage.size(); i++){
            System.out.println("WOOOOFFFF!!!");
        }
    }
    public String toString(){
        return "CAGE:\n"+
                "---------------------------\n"+
                "||                       "+ " ||\n"+
                "|| Dogs captured: "+cage.size()+"       ||\n"+
                "||                       "+ " ||\n"+
                "---------------------------";
    }
}
