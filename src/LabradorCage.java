import java.lang.reflect.Array;
import java.util.ArrayList;

public class LabradorCage{
    ArrayList<Labrador> cage = new ArrayList<Labrador>();
    public LabradorCage(){
        
    }
    public void add(Labrador dog){
        cage.add(dog);
    }
    public void remove(Labrador dog){
        cage.remove(dog);
    }
    public void chaos(){
        for (int i=0; i<cage.size(); i++){
            System.out.println("WOOOOFFFF!!!");
        }
    }
    public String toString(){
        return "CAGE:\n"+
                "----------------------------\n"+
                "||                       "+ " ||\n"+
                "|| Dogs captured: "+cage.size()+"       ||\n"+
                "||                       "+ " ||\n"+
                "----------------------------";
    }
}
